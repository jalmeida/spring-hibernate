package com.james.almeida.springhibernate.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.james.almeida.springhibernate.models.User;

public interface UserRepository extends CrudRepository<User, Long>{

	public Optional<User> findByUsername(String username);
	
}
