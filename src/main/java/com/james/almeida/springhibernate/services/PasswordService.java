package com.james.almeida.springhibernate.services;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class PasswordService {
	BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
	
	public String generateHash(String pass) {
		return encoder.encode(pass);
	}
	
	public boolean verifyhash(String pass, String hash){
		return encoder.matches(pass, hash);
	}
	
}
