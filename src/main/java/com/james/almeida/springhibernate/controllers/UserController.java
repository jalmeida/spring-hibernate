package com.james.almeida.springhibernate.controllers;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.james.almeida.springhibernate.models.User;
import com.james.almeida.springhibernate.repositories.UserRepository;
import com.james.almeida.springhibernate.services.PasswordService;
import com.james.almeida.springhibernate.services.TokenService;

@RestController
public class UserController {
	
	@Autowired
	PasswordService passwordService;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	TokenService tokenService;
	
	@RequestMapping(method=RequestMethod.POST, path="/user")
	public User createUser(@Valid @RequestBody User user) {
		
		String hash = passwordService.generateHash(user.getPassword());
		
		user.setPassword(hash);
		
		userRepository.save(user);
		
		return user;
		
	}
	
	@RequestMapping(method=RequestMethod.POST, path="/login")
	public ResponseEntity<?> doLogin(@RequestBody User user){
		
		Optional<User> userDBOptional = userRepository.findByUsername(user.getUsername());
		
		if(!userDBOptional.isPresent()) {
			return ResponseEntity.badRequest().build();
		} 
		
		User userDB = userDBOptional.get();
		
		if(passwordService.verifyhash(user.getPassword(), userDB.getPassword())) {
			
			String token = tokenService.generateToken(userDB.getUsername());
			
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", String.format("Bearer %s", token));
			headers.add("Access-Control-Expose-Headers", "Authorization");
			
			return new ResponseEntity<User>(userDB, headers, HttpStatus.OK);
		}
		
		return ResponseEntity.badRequest().build();
	}
	
}
